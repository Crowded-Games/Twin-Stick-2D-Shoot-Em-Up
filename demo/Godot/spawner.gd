extends Spawner

@export var Size: float = 1
@export var Health: int = 1
@export var Value: int = 1
@export var tex: Texture2D 

func _on_spawn():
	var temp = Enemy.new()
	temp.global_position = self.global_position + Vector2(randf_range(-GetSizeX(), GetSizeX()), randf_range(-GetSizeY(), GetSizeY()))
	temp.VariableHealth = Health
	temp.DragsAudio = ResourceLoader.load("res://Non-Godot/Kill.wav")
	temp.VariableScoreValue = Value
	temp.scale = Vector2(Size, Size);
	temp.connect("AddScore", Callable(get_parent(), "ScoreAdd"))
	temp.add_to_group("Enemy")
	# The randomization at the end is so that way the collisions don't go fucky wucky
	temp.DragsTexture = tex
	add_sibling(temp)
