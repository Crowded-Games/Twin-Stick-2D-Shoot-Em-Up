extends Player

func _on_respawn() -> void:
	get_tree().reload_current_scene()

func _ready() -> void:
	SetHit(ResourceLoader.load("res://Non-Godot/Hurt.wav"))
	SetPoof(ResourceLoader.load("res://Non-Godot/Poof.wav"))
	SetBulletTexture(ResourceLoader.load("res://Non-Godot/Bullet.png"))
