extends Area2D

func _on_body_entered(body):
	if body.is_in_group("Player"):
		body.SetPowerUp(0);
		self.queue_free()
