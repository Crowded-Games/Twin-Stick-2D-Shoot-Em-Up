extends Spawner

@export var Coin: PackedScene
@export var PowerUp: PackedScene
@export var power_spawn_rate: float = 20
var power_timer: float = 0

func _process(delta: float) -> void:
	power_timer += delta

func _on_spawn() -> void:
	var temp = Coin.instantiate()
	temp.position = self.position + Vector2(randf_range(-GetSizeX(), GetSizeX()), randf_range(-GetSizeY(), GetSizeY()))
	add_sibling.call_deferred(temp)
	
	if power_timer >= power_spawn_rate:
		power_timer = 0
		temp = PowerUp.instantiate()
		temp.position = self.position + Vector2(randf_range(-GetSizeX(), GetSizeX()), randf_range(-GetSizeY(), GetSizeY()))
		add_sibling.call_deferred(temp)
