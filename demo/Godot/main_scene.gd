extends Node2D

var Score = 0
var Coin = 0

func _process(_delta):
	get_node("Score").text = "Score: " + str(Score) + "\nCoins: " + str(Coin)

func ScoreAdd(score):
	Score += score
