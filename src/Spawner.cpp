#include "Spawner.hpp"

#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/core/memory.hpp>
#include <godot_cpp/classes/engine.hpp>
#include <godot_cpp/variant/utility_functions.hpp>

namespace godot {
	Spawner::Spawner() {
		
	}
	
	Spawner::~Spawner() {
		queue_free();
	}
	
	void Spawner::_bind_methods() {
		// Numbers
		ClassDB::bind_method(D_METHOD("GetTimerRandomization"), &Spawner::GetTimerRandomization);
		ClassDB::bind_method(D_METHOD("SetTimerRandomization", "TimerRandomization"), &Spawner::SetTimerRandomization);
		ClassDB::add_property("Spawner", PropertyInfo(Variant::BOOL, "TimerRandomization"), "SetTimerRandomization", "GetTimerRandomization");
		ClassDB::bind_method(D_METHOD("GetSpawnInterval"), &Spawner::GetSpawnInterval);
		ClassDB::bind_method(D_METHOD("SetSpawnInterval", "SpawnInterval"), &Spawner::SetSpawnInterval);
		ClassDB::add_property("Spawner", PropertyInfo(Variant::FLOAT, "SpawnInterval"), "SetSpawnInterval", "GetSpawnInterval");
		ClassDB::bind_method(D_METHOD("GetSizeX"), &Spawner::GetSizeX);
		ClassDB::bind_method(D_METHOD("SetSizeX", "SizeX"), &Spawner::SetSizeX);
		ClassDB::add_property("Spawner", PropertyInfo(Variant::FLOAT, "SizeX"), "SetSizeX", "GetSizeX");
		ClassDB::bind_method(D_METHOD("GetSizeY"), &Spawner::GetSizeY);
		ClassDB::bind_method(D_METHOD("SetSizeY", "SizeY"), &Spawner::SetSizeY);
		ClassDB::add_property("Spawner", PropertyInfo(Variant::FLOAT, "SizeY"), "SetSizeY", "GetSizeY");
		
		// Signals
		ClassDB::add_signal("Spawner", MethodInfo("Spawn"));
	}
	
	void Spawner::_physics_process(double delta) {
		// Stop running in certain circumstances
		if (Engine::get_singleton()->is_editor_hint()) {
			return;
		}
		
		timer += delta;
		if (timer >= RealSpawnInterval) {
			emit_signal("Spawn");
			timer = 0;
			if (TimerRandomization == true) {
				RealSpawnInterval = SpawnInterval + UtilityFunctions::randf_range(-0.75, 0.75);
			}
		}
	}
}
