#ifndef BULLET_HPP
#define BULLET_HPP

#include <godot_cpp/classes/area2d.hpp>
#include <godot_cpp/classes/cpu_particles2d.hpp>
#include <godot_cpp/classes/audio_stream_player.hpp>
#include <godot_cpp/classes/audio_stream.hpp>
#include <godot_cpp/classes/sprite2d.hpp>
#include <godot_cpp/classes/texture2d.hpp>

namespace godot {
	
	class Bullet : public Area2D {
		GDCLASS(Bullet, Area2D);
		
	public:
		float Speed = 800;
		Vector2 AreaDirection = Vector2(0, 0);
		bool Debounce = false;
		Ref<AudioStream> PoofAudioClip;
		Ref<AudioStream> HitAudioClip;
		Ref<Texture2D> Texture;
		
		Sprite2D* Sprite;
		CPUParticles2D* PoofParticles;
		AudioStreamPlayer* PoofAudio;
		CPUParticles2D* HitParticles;
		AudioStreamPlayer* HitAudio;
		
		Bullet();
		~Bullet();
		
		static void _bind_methods();
		void _physics_process(double delta) override;
		
		// Standalone methods
		void BodyEntered(Node2D* body);
		
		// Gets and Sets
		Ref<AudioStream> GetPoofAudio() const {
			return PoofAudioClip;
		}
		Ref<AudioStream> GetHitAudio() const {
			return PoofAudioClip;
		}
		Ref<Texture2D> GetTexture() const {
			return Texture;
		}
		
		void SetPoofAudio(const Ref<AudioStream>& audio ) {
			PoofAudioClip = audio;
			PoofAudio->set_stream(PoofAudioClip);
		}
		void SetHitAudio(const Ref<AudioStream>& audio ) {
			HitAudioClip = audio;
			HitAudio->set_stream(HitAudioClip);
		}
		void SetTexture(const Ref<Texture2D>& texture ) {
			Texture = texture;
			Sprite->set_texture(Texture);
		}
	};
}

#endif
