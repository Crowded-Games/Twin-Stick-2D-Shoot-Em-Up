#ifndef REGISTER_TYPES_HPP
#define REGISTER_TYPES_HPP

#include <godot_cpp/core/class_db.hpp>

using namespace godot;

void InitializeModule(ModuleInitializationLevel p_level);
void UninitializeModule(ModuleInitializationLevel p_level);

#endif
