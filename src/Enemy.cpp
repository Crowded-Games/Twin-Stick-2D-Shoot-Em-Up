#include "Enemy.hpp"

#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/core/property_info.hpp>
#include <godot_cpp/core/memory.hpp>
#include <godot_cpp/classes/engine.hpp>

#include <godot_cpp/classes/collision_shape2d.hpp>
#include <godot_cpp/classes/circle_shape2d.hpp>
#include <godot_cpp/classes/area2d.hpp>
#include <godot_cpp/classes/scene_tree.hpp>
#include <godot_cpp/classes/gradient.hpp>
#include <godot_cpp/variant/signal.hpp>

namespace godot {
	Enemy::Enemy() {
		// Other nodes to be attached.
		CollisionShape2D* Collision = memnew(CollisionShape2D);
		CircleShape2D* CollisionShape = memnew(CircleShape2D);
		Area2D* Area = memnew(Area2D);
		CollisionShape2D* Collision2 = memnew(CollisionShape2D);
		CircleShape2D* CollisionShape2 = memnew(CircleShape2D);
		Sprite = memnew(Sprite2D);
		Particles = memnew(CPUParticles2D);
		Gradient* ParticleGradient = memnew(Gradient);
		Audio = memnew(AudioStreamPlayer);
		
		// Assigning things.
		Collision->set_shape(CollisionShape);
		
		CollisionShape2->set_radius(12.0);
		Collision2->set_shape(CollisionShape2);
		
		Particles->set_emitting(false);
		Particles->set_amount(20);
		Particles->set_lifetime(0.5);
		Particles->set_one_shot(true);
		Particles->set_explosiveness_ratio(1.0);
		Particles->set_spread(180);
		Particles->set_gravity(Vector2(0, 0));
		Particles->set_param_min(CPUParticles2D::PARAM_INITIAL_LINEAR_VELOCITY, 40);
		Particles->set_param_max(CPUParticles2D::PARAM_INITIAL_LINEAR_VELOCITY, 110);
		Particles->set_param_min(CPUParticles2D::PARAM_ANGULAR_VELOCITY, 360);
		Particles->set_param_min(CPUParticles2D::PARAM_LINEAR_ACCEL, -200);
		Particles->set_param_max(CPUParticles2D::PARAM_LINEAR_ACCEL, -200);
		Particles->set_param_min(CPUParticles2D::PARAM_SCALE, 5);
		Particles->set_param_max(CPUParticles2D::PARAM_SCALE, 8);
		Color color1 = {0.75, 0, 0.13, 1};
		Color color2 = {1, 0.4, 0.4, 1};
		Color color3 = {1, 0.45, 0.4, 0};
		PackedFloat32Array offset = {0, 0.5, 1};
		ParticleGradient->set_offsets(offset);
		ParticleGradient->set_colors({color1, color2, color3});
		Particles->set_color_ramp(ParticleGradient);
		
		// Add children
		add_child(Collision);
		add_child(Area);
		Area->add_child(Collision2);
		Area->connect("body_entered", Callable(this, "DetectorBodyEnter"));
		add_child(Sprite);
		add_child(Particles);
		add_child(Audio);
	}
	
	Enemy::~Enemy() {
		queue_free();
	}
	
	void Enemy::_physics_process(double delta) {
		// Stop running in certain circumstances
		if (Engine::get_singleton()->is_editor_hint()) {
			return;
		}
		
		// Follow the player
		auto tree = get_tree();
		Array player = tree->get_nodes_in_group("Player");
		if (player.size() > 0) {
			look_at(player[0].get("position"));
			set_velocity(Vector2(0, 0));
			set_position(get_position().move_toward(player[0].get("position"), Speed * delta));
		}
		
		// Death
		if (Health <= 0) {
			Die();
		}
		
		move_and_slide();
	}
	
	void Enemy::_bind_methods() {
		ClassDB::add_property_group("Enemy", "Variables", "Variable");
		ClassDB::bind_method(D_METHOD("GetSpeed"), &Enemy::GetSpeed);
		ClassDB::bind_method(D_METHOD("SetSpeed", "Speed"), &Enemy::SetSpeed);
		ClassDB::add_property("Enemy", PropertyInfo(Variant::FLOAT, "VariableSpeed"), "SetSpeed", "GetSpeed");
		ClassDB::bind_method(D_METHOD("GetHealth"), &Enemy::GetHealth);
		ClassDB::bind_method(D_METHOD("SetHealth", "Health"), &Enemy::SetHealth);
		ClassDB::add_property("Enemy", PropertyInfo(Variant::INT, "VariableHealth"), "SetHealth", "GetHealth");
		ClassDB::bind_method(D_METHOD("GetScoreValue"), &Enemy::GetScoreValue);
		ClassDB::bind_method(D_METHOD("SetScoreValue", "ScoreValue"), &Enemy::SetScoreValue);
		ClassDB::add_property("Enemy", PropertyInfo(Variant::INT, "VariableScoreValue"), "SetScoreValue", "GetScoreValue");
		
		ClassDB::add_property_group("Enemy", "Drag&Drops", "Drags");
		ClassDB::bind_method(D_METHOD("GetTexture"), &Enemy::GetTexture);
		ClassDB::bind_method(D_METHOD("SetTexture", "Texture"), &Enemy::SetTexture);
		ClassDB::add_property("Enemy", PropertyInfo(Variant::OBJECT, "DragsTexture"), "SetTexture", "GetTexture");
		ClassDB::bind_method(D_METHOD("GetDeathAudio"), &Enemy::GetDeathAudio);
		ClassDB::bind_method(D_METHOD("SetDeathAudio", "Audio"), &Enemy::SetDeathAudio);
		ClassDB::add_property("Enemy", PropertyInfo(Variant::OBJECT, "DragsAudio"), "SetDeathAudio", "GetDeathAudio");
		
		// Standalone functions
		ClassDB::bind_method(D_METHOD("DetectorBodyEnter"), &Enemy::DetectorBodyEnter);
		
		// Signals
		ClassDB::add_signal("Enemy", MethodInfo("AddScore", PropertyInfo(Variant::INT, "Score")));
	}
	
	void Enemy::DetectorBodyEnter(Node2D* body) {
		if (body->is_in_group("Player")) {
			body->call("Die");
		}
	}
	
	void Enemy::Die() {
		Particles->set_emitting(true);
		Particles->reparent(get_parent());
		Audio->play();
		Audio->reparent(get_parent());
		emit_signal("AddScore", ScoreValue);
		queue_free();
	}
}
