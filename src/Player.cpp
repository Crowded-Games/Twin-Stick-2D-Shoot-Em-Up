#include "Player.hpp"
#include "Bullet.hpp"

#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/core/memory.hpp>
#include <godot_cpp/classes/engine.hpp>
#include <godot_cpp/variant/utility_functions.hpp>

#include <godot_cpp/classes/collision_shape2d.hpp>
#include <godot_cpp/classes/circle_shape2d.hpp>
#include <godot_cpp/classes/cpu_particles2d.hpp>
#include <godot_cpp/classes/gradient.hpp>

namespace godot {
	Player::Player() {
		// Other nodes to be attached.
		CollisionShape2D* Collision = memnew(CollisionShape2D);
		CircleShape2D* CollisionShape = memnew(CircleShape2D);
		Sprite = memnew(Sprite2D);
		Marker = memnew(Marker2D);
		Camera = memnew(Camera2D);
		Particles = memnew(CPUParticles2D);
		Gradient* ParticleGradient = memnew(Gradient);
		Audio = memnew(AudioStreamPlayer);
		
		// Assigning stuff.
		Collision->set_shape(CollisionShape);
		
		Marker->set_position(Vector2(15, 0));
		
		Particles->set_emitting(false);
		Particles->set_amount(450);
		Particles->set_lifetime(2);
		Particles->set_one_shot(true);
		Particles->set_spread(180);
		Particles->set_gravity(Vector2(0, 0));
		Particles->set_param_min(CPUParticles2D::PARAM_INITIAL_LINEAR_VELOCITY, 70);
		Particles->set_param_max(CPUParticles2D::PARAM_INITIAL_LINEAR_VELOCITY, 210);
		Particles->set_param_max(CPUParticles2D::PARAM_ANGULAR_VELOCITY, 360);
		Particles->set_param_min(CPUParticles2D::PARAM_SCALE, 10);
		Particles->set_param_max(CPUParticles2D::PARAM_SCALE, 15);
		Color color1 = {1, 1, 0, 1};
		Color color2 = {0, 0, 0, 0};
		ParticleGradient->set_colors({color1, color2});
		Particles->set_color_ramp(ParticleGradient);
		
		Camera->set_position(Vector2(100, 0));
		Camera->set_position_smoothing_enabled(true);
		
		// Complete the player.
		add_child(Collision);
		add_child(Sprite);
		add_child(Marker);
		add_child(Camera);
		add_child(Particles);
		add_child(Audio);
	}

	Player::~Player() {
		queue_free();
	}
	
	void Player::_bind_methods() {
		// Numbers
		ClassDB::add_property_group("Player", "Variables", "Variable");
		ClassDB::bind_method(D_METHOD("GetSpeed"), &Player::GetSpeed);
		ClassDB::bind_method(D_METHOD("SetSpeed", "Speed"), &Player::SetSpeed);
		ClassDB::add_property("Player", PropertyInfo(Variant::FLOAT, "VariableSpeed"), "SetSpeed", "GetSpeed");
		ClassDB::bind_method(D_METHOD("GetFireRate"), &Player::GetFireRate);
		ClassDB::bind_method(D_METHOD("SetFireRate", "FireRate"), &Player::SetFireRate);
		ClassDB::add_property("Player", PropertyInfo(Variant::FLOAT, "VariableFireRate"), "SetFireRate", "GetFireRate");
		ClassDB::bind_method(D_METHOD("GetDead"), &Player::GetDead);
		ClassDB::bind_method(D_METHOD("SetDead", "IsDead"), &Player::SetDead);
		ClassDB::add_property("Player", PropertyInfo(Variant::BOOL, "VariableIsDead"), "SetDead", "GetDead");
		
		// Drag & Drops
		ClassDB::add_property_group("Player", "Drag&Drops", "Drags");
		ClassDB::bind_method(D_METHOD("GetTexture"), &Player::GetTexture);
		ClassDB::bind_method(D_METHOD("SetTexture", "Texture"), &Player::SetTexture);
		ClassDB::add_property("Player", PropertyInfo(Variant::OBJECT, "DragsTexture"), "SetTexture", "GetTexture");
		ClassDB::bind_method(D_METHOD("GetDeathAudio"), &Player::GetDeathAudio);
		ClassDB::bind_method(D_METHOD("SetDeathAudio", "DeathAudio"), &Player::SetDeathAudio);
		ClassDB::add_property("Player", PropertyInfo(Variant::OBJECT, "DragsDeathAudio"), "SetDeathAudio", "GetDeathAudio");
		
		// Inputs
		ClassDB::add_property_group("Player", "Input Map Names", "Input");
		ClassDB::bind_method(D_METHOD("GetLeft"), &Player::GetLeft);
		ClassDB::bind_method(D_METHOD("SetLeft", "Left"), &Player::SetLeft);
		ClassDB::add_property("Player", PropertyInfo(Variant::STRING_NAME, "InputLeft"), "SetLeft", "GetLeft");
		ClassDB::bind_method(D_METHOD("GetUp"), &Player::GetUp);
		ClassDB::bind_method(D_METHOD("SetUp", "Up"), &Player::SetUp);
		ClassDB::add_property("Player", PropertyInfo(Variant::STRING_NAME, "InputUp"), "SetUp", "GetUp");
		ClassDB::bind_method(D_METHOD("GetRight"), &Player::GetRight);
		ClassDB::bind_method(D_METHOD("SetRight", "Right"), &Player::SetRight);
		ClassDB::add_property("Player", PropertyInfo(Variant::STRING_NAME, "InputRight"), "SetRight", "GetRight");
		ClassDB::bind_method(D_METHOD("GetDown"), &Player::GetDown);
		ClassDB::bind_method(D_METHOD("SetDown", "Down"), &Player::SetDown);
		ClassDB::add_property("Player", PropertyInfo(Variant::STRING_NAME, "InputDown"), "SetDown", "GetDown");
		ClassDB::bind_method(D_METHOD("GetShoot"), &Player::GetShoot);
		ClassDB::bind_method(D_METHOD("SetShoot", "Shoot"), &Player::SetShoot);
		ClassDB::add_property("Player", PropertyInfo(Variant::STRING_NAME, "InputShoot"), "SetShoot", "GetShoot");
		ClassDB::bind_method(D_METHOD("GetRespawn"), &Player::GetRespawn);
		ClassDB::bind_method(D_METHOD("SetRespawn", "Respawn"), &Player::SetRespawn);
		ClassDB::add_property("Player", PropertyInfo(Variant::STRING_NAME, "InputRespawn"), "SetRespawn", "GetRespawn");
		
		ClassDB::bind_method(D_METHOD("GetPowerUp"), &Player::GetPowerUp);
		ClassDB::bind_method(D_METHOD("SetPowerUp", "PowerUp"), &Player::SetPowerUp);
		ClassDB::add_property("Player", PropertyInfo(Variant::STRING, "PowerUp"), "SetPowerUp", "GetPowerUp");
		
		// Signals
		ClassDB::add_signal("Player", MethodInfo("Respawn"));
		
		// Standalone Functions
		ClassDB::bind_method(D_METHOD("Die"), &Player::Die);
		
		// WORKAROUND: yeah.
		ClassDB::bind_method(D_METHOD("SetPoof"), &Player::SetPoof);
		ClassDB::bind_method(D_METHOD("SetHit"), &Player::SetHit);
		ClassDB::bind_method(D_METHOD("SetBulletTexture"), &Player::SetBulletTexture);
	}
	
	void Player::_physics_process(double delta) {
		// Stop running in certain circumstances
		if (Engine::get_singleton()->is_editor_hint()) {
			return;
		}
		if (IsDead == true) {
			set_velocity(Vector2(0, 0));
			// Respawn stuff
			if (Input::get_singleton()->is_action_just_pressed(Respawn)) {
				emit_signal("Respawn");
			}
			return;
		}
		timer += delta;
		
		// Power Up things
		float RealFireRate = FireRate;
		if (PowerUpEnable == true) {
			PowerUpTimer += delta;
			RealFireRate /= 2;
			// Disable powerups
			if (PowerUpTimer >= 10) {
				PowerUpEnable = false;
				PowerUpTimer = 0;
			}
		}
		
		// Movement
		float AxisX = Input::get_singleton()->get_axis(Left, Right);
		float AxisY = Input::get_singleton()->get_axis(Up, Down);
		set_velocity(Vector2(AxisX * Speed, AxisY * Speed));
		
		// Shooting
		if (Input::get_singleton()->get_action_strength(Shoot) >= 1 && timer >= RealFireRate) {
			Bullet* bullet = memnew(Bullet);
			bullet->AreaDirection = (get_global_mouse_position() - get_global_position()).normalized();
			bullet->set_global_position(Marker->get_global_position());
			bullet->SetPoofAudio(PoofSound);
			bullet->SetHitAudio(HitSound);
			bullet->SetTexture(BulletTexture);
			add_sibling(bullet);
			Player::Camera->set_offset(Vector2(UtilityFunctions::randf_range(-4, 4), UtilityFunctions::randf_range(-4, 4)));
			timer = 0;
		}
		else {
			// Stop wiggling the camera if there is no shooting
			Player::Camera->set_offset(Vector2(0, 0));
		}
		
		move_and_slide();
		look_at(get_global_mouse_position());
	}
	
	void Player::Die() {
		if (IsDead == false) {
			Particles->reparent(get_parent());
			Particles->set_emitting(true);
			Audio->play();
			Camera->set_position(Vector2(0, 0));
			set_visible(false);
			IsDead = true;
		}
	}
}
