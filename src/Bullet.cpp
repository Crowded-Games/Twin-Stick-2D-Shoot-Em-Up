#include "Bullet.hpp"

#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/core/property_info.hpp>
#include <godot_cpp/core/memory.hpp>

#include <godot_cpp/classes/collision_shape2d.hpp>
#include <godot_cpp/classes/circle_shape2d.hpp>
#include <godot_cpp/classes/gradient.hpp>

namespace godot {
	Bullet::Bullet() {
		set_scale(Vector2(0.5, 0.5));
		
		CollisionShape2D* Collision = memnew(CollisionShape2D);
		CircleShape2D* CollisionShape = memnew(CircleShape2D);
		Sprite = memnew(Sprite2D);
		PoofParticles = memnew(CPUParticles2D);
		PoofAudio = memnew(AudioStreamPlayer);
		Gradient* PoofGradient = memnew(Gradient);
		HitParticles = memnew(CPUParticles2D);
		HitAudio = memnew(AudioStreamPlayer);
		Gradient* HitGradient = memnew(Gradient);
		
		Collision->set_shape(CollisionShape);
		
		Color color1 = {1, 1, 1, 1};
		Color color2 = {0.25, 0.25, 0.25, 0};
		
		PoofParticles->set_emitting(false);
		PoofParticles->set_amount(4);
		PoofParticles->set_lifetime(0.25);
		PoofParticles->set_one_shot(true);
		PoofParticles->set_explosiveness_ratio(1.0);
		PoofParticles->set_spread(180);
		PoofParticles->set_gravity(Vector2(0, 0));
		PoofParticles->set_param_min(CPUParticles2D::PARAM_INITIAL_LINEAR_VELOCITY, 30);
		PoofParticles->set_param_max(CPUParticles2D::PARAM_INITIAL_LINEAR_VELOCITY, 30);
		PoofParticles->set_param_max(CPUParticles2D::PARAM_ANGULAR_VELOCITY, 360);
		PoofParticles->set_param_min(CPUParticles2D::PARAM_SCALE, 5);
		PoofParticles->set_param_max(CPUParticles2D::PARAM_SCALE, 5);
		PoofGradient->set_colors({color1, color2});
		PoofParticles->set_color_ramp(PoofGradient);
		
		HitParticles->set_emitting(false);
		HitParticles->set_amount(4);
		HitParticles->set_lifetime(0.25);
		HitParticles->set_one_shot(true);
		HitParticles->set_explosiveness_ratio(1.0);
		HitParticles->set_spread(180);
		HitParticles->set_gravity(Vector2(0, 0));
		HitParticles->set_param_min(CPUParticles2D::PARAM_INITIAL_LINEAR_VELOCITY, 30);
		HitParticles->set_param_max(CPUParticles2D::PARAM_INITIAL_LINEAR_VELOCITY, 30);
		HitParticles->set_param_max(CPUParticles2D::PARAM_ANGULAR_VELOCITY, 360);
		HitParticles->set_param_min(CPUParticles2D::PARAM_SCALE, 5);
		HitParticles->set_param_max(CPUParticles2D::PARAM_SCALE, 5);
		HitGradient->set_colors({color1, color2});
		HitParticles->set_color_ramp(HitGradient);
		
		// add teh children
		add_child(Collision);
		connect("body_entered", Callable(this, "BodyEntered"));
		add_child(Sprite);
		add_child(PoofParticles);
		add_child(PoofAudio);
		add_child(HitParticles);
		add_child(HitAudio);
	}
	
	Bullet::~Bullet() {
		queue_free();
	}
	
	void Bullet::_bind_methods() {
		ClassDB::bind_method(D_METHOD("GetPoofAudio"), &Bullet::GetPoofAudio);
		ClassDB::bind_method(D_METHOD("SetPoofAudio", "PoofAudio"), &Bullet::SetPoofAudio);
		ClassDB::add_property("Bullet", PropertyInfo(Variant::OBJECT, "PoofAudio"), "SetPoofAudio", "GetPoofAudio");
		ClassDB::bind_method(D_METHOD("GetHitAudio"), &Bullet::GetHitAudio);
		ClassDB::bind_method(D_METHOD("SetHitAudio", "HitAudio"), &Bullet::SetHitAudio);
		ClassDB::add_property("Bullet", PropertyInfo(Variant::OBJECT, "HitAudio"), "SetHitAudio", "GetHitAudio");
		ClassDB::bind_method(D_METHOD("GetTexture"), &Bullet::GetTexture);
		ClassDB::bind_method(D_METHOD("SetTexture", "Texture"), &Bullet::SetTexture);
		ClassDB::add_property("Bullet", PropertyInfo(Variant::OBJECT, "Texture"), "SetTexture", "GetTexture");
		
		// Standalone Method
		ClassDB::bind_method(D_METHOD("BodyEntered"), &Bullet::BodyEntered);
	}
	
	void Bullet::_physics_process(double delta) {
		translate(AreaDirection * Speed * delta);
	}
	
	void Bullet::BodyEntered(Node2D* body) {
		if (Debounce == true) {
			return;
		}
		// Don't destroy walls or players, only enemies
		if (body->is_in_group("Player")) {
			return;
		}
		else if (body->is_in_group("Enemy")) {
			body->call("SetHealth", int(body->call("GetHealth")) - 1);
			HitParticles->set_emitting(true);
			HitParticles->reparent(get_parent());
			HitAudio->play();
			HitAudio->reparent(get_parent());
			queue_free();
		}
		else {
			PoofParticles->set_emitting(true);
			PoofParticles->reparent(get_parent());
			PoofAudio->play();
			PoofAudio->reparent(get_parent());
			queue_free();
		}
	}
}
