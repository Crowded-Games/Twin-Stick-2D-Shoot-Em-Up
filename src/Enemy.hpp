#ifndef ENEMY_HPP
#define ENEMY_HPP

#include <godot_cpp/classes/character_body2d.hpp>
#include <godot_cpp/classes/sprite2d.hpp>
#include <godot_cpp/classes/texture2d.hpp>
#include <godot_cpp/classes/cpu_particles2d.hpp>
#include <godot_cpp/classes/audio_stream.hpp>
#include <godot_cpp/classes/audio_stream_player.hpp>
#include <godot_cpp/classes/node2d.hpp>

namespace godot {
	
	class Enemy : public CharacterBody2D {
		GDCLASS(Enemy, CharacterBody2D);
		
	public:
		float Speed = 100;
		int Health = 1;
		int ScoreValue = 1;
		Ref<Texture2D> Texture;
		Ref<AudioStream> DeathAudio;
		
		Enemy();
		~Enemy();
		
		// Nodes
		Sprite2D* Sprite;
		CPUParticles2D* Particles;
		AudioStreamPlayer* Audio;
		
		void _physics_process(double delta) override;
		static void _bind_methods();
		void DetectorBodyEnter(Node2D* body);
		void Die();
		
		// Getters and Setters
		float GetSpeed() const {
			return Speed;
		}
		int GetHealth() const {
			return Health;
		}
		int GetScoreValue() const {
			return ScoreValue;
		}
		Ref<Texture2D> GetTexture() const {
			return Texture;
		}
		Ref<AudioStream> GetDeathAudio() const {
			return DeathAudio;
		}
		
		void SetSpeed(const float speed) {
			Speed = speed;
		}
		void SetHealth(const int health) {
			Health = health;
		}
		void SetScoreValue(const int score) {
			ScoreValue = score;
		}
		void SetTexture(const Ref<Texture2D>& texture) {
			Texture = texture;
			Sprite->set_texture(Texture);
		}
		void SetDeathAudio(const Ref<AudioStream>& audio) {
			DeathAudio = audio;
			Audio->set_stream(DeathAudio);
		}
	};
}

#endif
