#ifndef SPAWNER_HPP
#define SPAWNER_HPP

#include <godot_cpp/core/property_info.hpp>
#include <godot_cpp/classes/ref.hpp>

#include <godot_cpp/classes/marker2d.hpp>

namespace godot {
	
	class Spawner : public Marker2D {
		GDCLASS(Spawner, Marker2D);
		
	public:
		bool TimerRandomization = false;
		double SpawnInterval = 2.5;
		double RealSpawnInterval = SpawnInterval;
		double timer = 0.0;
		float SizeX = 0.0;
		float SizeY = 0.0;
		
		Spawner();
		~Spawner();
		
		void _physics_process(double delta) override;
		static void _bind_methods();
		
		// Gets and Sets
		bool GetTimerRandomization() const {
			return TimerRandomization;
		}
		double GetSpawnInterval() const {
			return SpawnInterval;
		}
		float GetSizeX() const {
			return SizeX;
		}
		float GetSizeY() const {
			return SizeY;
		}
		
		void SetTimerRandomization(const bool timerRandomization) {
			TimerRandomization = timerRandomization;
		}
		void SetSpawnInterval(const double spawnInterval) {
			SpawnInterval = spawnInterval;
			RealSpawnInterval = SpawnInterval;
		}
		void SetSizeX(const float sizeX) {
			SizeX = sizeX;
		}
		void SetSizeY(const float sizeY) {
			SizeY = sizeY;
		}
	};
}

#endif
