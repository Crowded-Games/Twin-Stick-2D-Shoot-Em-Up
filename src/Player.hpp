#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <godot_cpp/classes/input.hpp>
#include <godot_cpp/classes/ref.hpp>

#include <godot_cpp/classes/character_body2d.hpp>
#include <godot_cpp/classes/sprite2d.hpp>
#include <godot_cpp/classes/texture2d.hpp>
#include <godot_cpp/classes/marker2d.hpp>
#include <godot_cpp/classes/camera2d.hpp>
#include <godot_cpp/classes/cpu_particles2d.hpp>
#include <godot_cpp/classes/audio_stream.hpp>
#include <godot_cpp/classes/audio_stream_player.hpp>

namespace godot {

	class Player : public CharacterBody2D {
		GDCLASS(Player, CharacterBody2D)

	public:
		// General
		float Speed = 300;
		double FireRate = 0.2;
		bool IsDead = false;
		Ref<Texture2D> Texture;
		Ref<AudioStream> DeathAudio;
		
		// WORKAROUND: yeah.
		Ref<AudioStream> PoofSound;
		Ref<AudioStream> HitSound;
		Ref<Texture2D> BulletTexture;
		
		double timer = 0.0;
		double PowerUpTimer = 0.0;
		
		// PowerUps
		bool PowerUpEnable = false;
		enum PowerUps {
			DOUBLE_FIRERATE = 0,
			DOUBLE_POINTS = 1,
			DOUBLE_CASH = 2,
		};
		int CurrentPowerUp = DOUBLE_FIRERATE;
		
		// Input names
		StringName Left;
		StringName Up;
		StringName Right;
		StringName Down;
		StringName Shoot;
		StringName Respawn;
		
		// Nodes
		Sprite2D* Sprite;
		Marker2D* Marker;
		Camera2D* Camera;
		CPUParticles2D* Particles;
		AudioStreamPlayer* Audio;
		
		Player();
		~Player();
		
		// Godot Engine functions
		void _physics_process(double delta) override;
		static void _bind_methods();
		
		// Custom functions
		void Die();
		
		// Getters and Setters
		float GetSpeed() const {
			return Speed;
		}
		double GetFireRate() const {
			return FireRate;
		}
		Ref<Texture2D> GetTexture() const {
			return Texture;
		}
		bool GetDead() const {
			return IsDead;
		}
		Ref<AudioStream> GetDeathAudio() const {
			return DeathAudio;
		}
		int GetPowerUp() const {
			return CurrentPowerUp;
		}
		StringName GetLeft() const {
			return Left;
		}
		StringName GetUp() const {
			return Up;
		}
		StringName GetRight() const {
			return Right;
		}
		StringName GetDown() const {
			return Down;
		}
		StringName GetShoot() const {
			return Shoot;
		}
		StringName GetRespawn() const {
			return Respawn;
		}
		
		void SetSpeed(const float speed) {
			Speed = speed;
		}
		void SetFireRate(const double firerate) {
			FireRate = firerate;
		}
		void SetTexture(const Ref<Texture2D>& texture) {
			Texture = texture;
			Sprite->set_texture(Texture);
		}
		void SetDead(const bool dead) {
			IsDead = dead;
		}
		void SetDeathAudio(const Ref<AudioStream>& audio) {
			DeathAudio = audio;
			Audio->set_stream(DeathAudio);
		}
		void SetPowerUp(const int PowerUp) {
			PowerUpEnable = true;
			CurrentPowerUp = PowerUp;
		}
		void SetLeft(const StringName left) {
			Left = left;
		}
		void SetUp(const StringName up) {
			Up = up;
		}
		void SetRight(const StringName right) {
			Right = right;
		}
		void SetDown(const StringName down) {
			Down = down;
		}
		void SetShoot(const StringName shoot) {
			Shoot = shoot;
		}
		void SetRespawn(const StringName respawn) {
			Respawn = respawn;
		}
		void SetHit(const Ref<AudioStream> audio) {
			HitSound = audio;
		}
		void SetPoof(const Ref<AudioStream> audio) {
			PoofSound = audio;
		}
		void SetBulletTexture(const Ref<Texture2D> audio) {
			BulletTexture = audio;
		}
	};
}

#endif
